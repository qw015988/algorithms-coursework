#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int counter = 0;

int main() {
    pid_t pid;
    // Fork a child process
    pid = fork();
    // Check forking error
    if (pid < 0) {
        printf("Fork failed\n");
        return 1;
    }
    // Parent process
    if (pid > 0) {
        for (int i = 0; i < 10; i++) {
            printf("Parent %d Process: counter=%d\n", getpid(), ++counter);
        }
    }
    // Child process
    else {
        for (int i = 0; i < 10; i++) {
            printf("Child %d Process: counter=%d\n", getpid(), ++counter);
        }
    }
    //printf("\n%d",counter);
    return 0;
}
