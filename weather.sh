echo "Enter the temperature in celsius: " #Prompting the user to enter the temperature
read temp #reading the temperature and saving the value to a variable named temp
if [ "$temp" -ge 25 ]; then #checking if temperature is greater than or equal to 25
	echo "Warm weather" #print warm weather if the temperature is greater than or equal to 25
else #else statement
	echo "Cool weather" #print cold weather if the temperature is not greater than or equal to 25
fi #end of if statement
