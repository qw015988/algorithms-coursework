#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#define N 3 /* define the total number of processes we want */
float total=0; /* Set global variable */
int compute (int pid){ /* compute function just does something. */
int i;
float result=0;
for(i=0; i < 2000000000 ; i++) /* for a large number of times */
result = sqrt(1000.0) * sqrt(1000.0);
printf("Result of %d is %f \n", pid, result); /* Prints the result */
total = total + result; /* to keep a running total in the global variable total */
printf("Total of %d is %f \n", pid, total); /* Print running total so far. */
return 0;
}
int main(){
int cid[N], i, pid;
for(i = 0; i < N; i++){ /* to loop and create the required number of processes */
if((cid[i]=fork())==-1) /* Do the fork and catch it if it/one fails */
exit(1);
else if(cid[i] > 0){ /* give a message about the proc ID */
pid = getpid();
printf("Process Id for process %d is %d", i, pid);
compute(pid); /* call the function to do some computation */
break; /* After computation, quit. OR process creation will boom! */
}
}
return 0;
}

